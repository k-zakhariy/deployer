<?php

// Determine where the configuration file is stored
define('DEPLOYER_CONFIGDIR', realpath(dirname(__FILE__).'/config/'));

// Start the deployer
require dirname(__FILE__).'/../../vendor/janklan/deployer/src/deploy.php';