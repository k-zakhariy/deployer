# Deployer Changelog

## 1.0.7 - 2016-04-22

 - Minor fixes of markdown output

## 1.0.6 - 2016-04-22

  - Config script name can now be passed through ?config= variable
  - Generated output is now in Markdown format
  - Improved the way how commands are executed

## 1.0.5 - 2016-02-14

  - Minor improvements
  
## 1.0.4 - 2015-11-05

  - Minor improvements 
 
## 1.0.3 - 2015-05-11

  - Minor improvements

## 1.0.2 - 2014-10-29

  - Moved configuration file outside composer package

## 1.0.1 - 2014-10-26

  - Minor bugfixes

## 1.0 - 2014-10-26

  - Initial release with basic ability to handle bitbucket POST hooks announcing
    a push in a repository. This version works only in environment, where the
    webserver user has full write rights to the directory structure of your
    website.